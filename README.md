# Shell commands #

* jobctl

# Features #

* Task handlers for Voodoo particles.
* ETL jobs for Hive particles.
* Build jobs via BuildBOt for Deming particles.

### List of packages ###

* behat
* buildbot
* jenkins
* selenium
* stanbol