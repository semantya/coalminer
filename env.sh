#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",octave,R"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
#fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" 'r-cran-*' `echo r-{base,doc-{html,intro,pdf},gnome,mathlib,non{linear,cran-{hmisc,lindsey}},other-{bio3d,genabel,mott-happy},recommended,revolution-revobase}`"
fi

export NPM_PKGs=$NPM_PKGs" ql.io-engine"

